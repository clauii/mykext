define INFO_PLIST
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>CFBundleDevelopmentRegion</key>
	<string>English</string>
	<key>CFBundleExecutable</key>
	<string>$(KEXTNAME)</string>
	<key>CFBundleIdentifier</key>
	<string>$(KEXTBUNDLEID)</string>
	<key>CFBundleInfoDictionaryVersion</key>
	<string>6.0</string>
	<key>CFBundlePackageType</key>
	<string>KEXT</string>
	<key>CFBundleSignature</key>
	<string>????</string>
	<key>CFBundleVersion</key>
	<string>$(KEXTVERSION)</string>
	<key>OSBundleEnableKextLogging</key>
	<true/>
	<key>OSBundleLibraries</key>
	<dict>
		<key>com.apple.kpi.libkern</key>
		<string>8.0</string>
		<key>com.apple.kpi.mach</key>
		<string>8.0</string>
	</dict>
</dict>
</plist>
endef

export INFO_PLIST

KEXTNAME=MyKext
KEXTPKGNAME=mykext
KEXTBUNDLEID=cat.claudi.kext.mykext
KEXTVERSION=0.1.0d1
KEXTRELEASE=1
ARCH=x86
INSTALL_DIR=/Library/Extensions

PKGID=$(KEXTPKGNAME)-$(KEXTVERSION)-$(KEXTRELEASE)
BINROOTDIR=$(ARCH)/bin/$(KEXTPKGNAME)/$(KEXTNAME).kext

SHELL=/bin/bash

.PHONY: build check check_env clean clean_binrootdir install \
	release run

# The first target in the file is the default target
run: check release

clean_binrootdir: check_env
	rm -rf "$(BINROOTDIR)"

$(BINROOTDIR): clean_binrootdir \
	$(patsubst src/%.c,$(BINROOTDIR)/Contents/MacOS/%,$(wildcard src/*.c)) \
	$(BINROOTDIR)/Contents/Info.plist

$(BINROOTDIR)/Contents/MacOS/%: src/%.c
	mkdir -p "$(dir $@)"
	xcrun clang \
		$< -fno-builtin -nostdlib -static \
		-lkmod -o $@ \
		-D KEXTBUNDLEID=$(KEXTBUNDLEID) -D KEXTVERSION=$(KEXTVERSION) \
		-I/System/Library/Frameworks/Kernel.framework/Headers \
		-Wall -Wl,-kext

$(BINROOTDIR)/Contents/Info.plist:
	mkdir -p "$(dir $@)"
	echo "$${INFO_PLIST}" >> $@

$(ARCH)/release/%/$(PKGID).tar.bz: check build
	mkdir -p "$(dir $@)"
	gtar -c -f $@ --bzip2 --no-xattrs \
		-C $(@:$(ARCH)/release/%/$(PKGID).tar.bz=$(ARCH)/bin/%) \
		--exclude '__MACOSX*' --exclude '*.DS_Store' \
		--group wheel --owner root \
		.

build: check src $(BINROOTDIR)

install: check
	gtar -x -f $(ARCH)/release/$(KEXTPKGNAME)/$(PKGID).tar.bz \
		-C $(INSTALL_DIR)
	touch $(INSTALL_DIR)

release: check \
	$(ARCH)/release/$(KEXTPKGNAME)/$(PKGID).tar.bz

check: check_env
	type -t gtar >/dev/null
	[[ -d '/System/Library/Frameworks/Kernel.framework/Headers' ]]

check_env:
	[[ "$(ARCH)" && "$(BINROOTDIR)" && "$(PKGID)" && "$(INSTALL_DIR)" \
		&& "$(KEXTNAME)" && "$(KEXTVERSION)" && "$(KEXTRELEASE)" \
		&& "$(KEXTPKGNAME)" ]]
	[[ "$(ARCH)$(BINROOTDIR)$(PKGID)$(INSTALL_DIR)$(KEXTNAME)$(KEXTVERSION)$(KEXTRELEASE)$(KEXTPKGNAME)" \
		!= *' '* ]]

clean: check_env clean_binrootdir
	rm -rf "$(ARCH)/release/$(KEXTPKGNAME)"
