#include <libkern/libkern.h>
#include <mach/mach_types.h>

#define __STR(a) #a

// De-stringify the name, stringify the version
#define __KMOD_EXPLICIT_DECL(name, version, start, stop) \
    KMOD_EXPLICIT_DECL(name, __STR(version), start, stop)

kern_return_t MyKextStart(kmod_info_t *ki, void *d) {
  printf("Hello, World!\n");
  return KERN_SUCCESS;
}

kern_return_t MyKextStop(kmod_info_t *ki, void *d) {
  printf("Goodbye, World!\n");
  return KERN_SUCCESS;
}

extern kern_return_t _start(kmod_info_t *ki, void *data);
extern kern_return_t _stop(kmod_info_t *ki, void *data);

__KMOD_EXPLICIT_DECL(KEXTBUNDLEID, KEXTVERSION, _start, _stop)

#pragma GCC visibility push(default)
kmod_start_func_t *_realmain = MyKextStart;
kmod_stop_func_t *_antimain = MyKextStop;
int _kext_apple_cc = __APPLE_CC__;
#pragma GCC visibility pop
